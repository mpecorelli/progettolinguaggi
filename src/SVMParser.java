// $ANTLR 3.5.2 C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g 2017-05-03 17:13:23

import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class SVMParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ADD", "AND", "BRANCH", "BRANCHEQ", 
		"BRANCHGREATEQ", "BRANCHLESSEQ", "COL", "COPYFP", "DIV", "ERR", "HALT", 
		"JS", "LABEL", "LOADFP", "LOADHP", "LOADRA", "LOADRV", "LOADW", "MULT", 
		"NOT", "NUMBER", "OR", "POP", "PRINT", "PUSH", "STOREFP", "STOREHP", "STORERA", 
		"STORERV", "STOREW", "SUB", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ADD=4;
	public static final int AND=5;
	public static final int BRANCH=6;
	public static final int BRANCHEQ=7;
	public static final int BRANCHGREATEQ=8;
	public static final int BRANCHLESSEQ=9;
	public static final int COL=10;
	public static final int COPYFP=11;
	public static final int DIV=12;
	public static final int ERR=13;
	public static final int HALT=14;
	public static final int JS=15;
	public static final int LABEL=16;
	public static final int LOADFP=17;
	public static final int LOADHP=18;
	public static final int LOADRA=19;
	public static final int LOADRV=20;
	public static final int LOADW=21;
	public static final int MULT=22;
	public static final int NOT=23;
	public static final int NUMBER=24;
	public static final int OR=25;
	public static final int POP=26;
	public static final int PRINT=27;
	public static final int PUSH=28;
	public static final int STOREFP=29;
	public static final int STOREHP=30;
	public static final int STORERA=31;
	public static final int STORERV=32;
	public static final int STOREW=33;
	public static final int SUB=34;
	public static final int WHITESP=35;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public SVMParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public SVMParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return SVMParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g"; }


	      
	    int[] code = new int[ExecuteVM.CODESIZE];    
	    private int i = 0;
	    private HashMap<String,Integer> labelAdd = new HashMap<String,Integer>();
	    private HashMap<Integer,String> labelRef = new HashMap<Integer,String>();
	        



	// $ANTLR start "assembly"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:24:1: assembly : ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | AND | OR | NOT | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | BRANCHGREATEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* ;
	public final void assembly() throws RecognitionException {
		Token n=null;
		Token l=null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:24:9: ( ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | AND | OR | NOT | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | BRANCHGREATEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:25:5: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | AND | OR | NOT | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | BRANCHGREATEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			{
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:25:5: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | AND | OR | NOT | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | BRANCHGREATEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			loop1:
			while (true) {
				int alt1=30;
				switch ( input.LA(1) ) {
				case PUSH:
					{
					int LA1_2 = input.LA(2);
					if ( (LA1_2==NUMBER) ) {
						alt1=1;
					}
					else if ( (LA1_2==LABEL) ) {
						alt1=2;
					}

					}
					break;
				case POP:
					{
					alt1=3;
					}
					break;
				case ADD:
					{
					alt1=4;
					}
					break;
				case SUB:
					{
					alt1=5;
					}
					break;
				case MULT:
					{
					alt1=6;
					}
					break;
				case DIV:
					{
					alt1=7;
					}
					break;
				case AND:
					{
					alt1=8;
					}
					break;
				case OR:
					{
					alt1=9;
					}
					break;
				case NOT:
					{
					alt1=10;
					}
					break;
				case STOREW:
					{
					alt1=11;
					}
					break;
				case LOADW:
					{
					alt1=12;
					}
					break;
				case LABEL:
					{
					alt1=13;
					}
					break;
				case BRANCH:
					{
					alt1=14;
					}
					break;
				case BRANCHEQ:
					{
					alt1=15;
					}
					break;
				case BRANCHLESSEQ:
					{
					alt1=16;
					}
					break;
				case BRANCHGREATEQ:
					{
					alt1=17;
					}
					break;
				case JS:
					{
					alt1=18;
					}
					break;
				case LOADRA:
					{
					alt1=19;
					}
					break;
				case STORERA:
					{
					alt1=20;
					}
					break;
				case LOADRV:
					{
					alt1=21;
					}
					break;
				case STORERV:
					{
					alt1=22;
					}
					break;
				case LOADFP:
					{
					alt1=23;
					}
					break;
				case STOREFP:
					{
					alt1=24;
					}
					break;
				case COPYFP:
					{
					alt1=25;
					}
					break;
				case LOADHP:
					{
					alt1=26;
					}
					break;
				case STOREHP:
					{
					alt1=27;
					}
					break;
				case PRINT:
					{
					alt1=28;
					}
					break;
				case HALT:
					{
					alt1=29;
					}
					break;
				}
				switch (alt1) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:25:7: PUSH n= NUMBER
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly42); 
					n=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_assembly46); 
					code[i++] = PUSH; 
								                 code[i++] = Integer.parseInt((n!=null?n.getText():null));
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:27:6: PUSH l= LABEL
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly57); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly61); 
					code[i++] = PUSH; //
						    		             labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:29:6: POP
					{
					match(input,POP,FOLLOW_POP_in_assembly81); 
					code[i++] = POP;
					}
					break;
				case 4 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:30:6: ADD
					{
					match(input,ADD,FOLLOW_ADD_in_assembly96); 
					code[i++] = ADD;
					}
					break;
				case 5 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:31:6: SUB
					{
					match(input,SUB,FOLLOW_SUB_in_assembly110); 
					code[i++] = SUB;
					}
					break;
				case 6 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:32:6: MULT
					{
					match(input,MULT,FOLLOW_MULT_in_assembly124); 
					code[i++] = MULT;
					}
					break;
				case 7 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:33:6: DIV
					{
					match(input,DIV,FOLLOW_DIV_in_assembly137); 
					code[i++] = DIV;
					}
					break;
				case 8 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:34:6: AND
					{
					match(input,AND,FOLLOW_AND_in_assembly151); 
					code[i++] = AND;
					}
					break;
				case 9 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:35:6: OR
					{
					match(input,OR,FOLLOW_OR_in_assembly166); 
					code[i++] = OR;
					}
					break;
				case 10 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:36:6: NOT
					{
					match(input,NOT,FOLLOW_NOT_in_assembly181); 
					code[i++] = NOT;
					}
					break;
				case 11 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:37:6: STOREW
					{
					match(input,STOREW,FOLLOW_STOREW_in_assembly196); 
					code[i++] = STOREW;
					}
					break;
				case 12 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:38:6: LOADW
					{
					match(input,LOADW,FOLLOW_LOADW_in_assembly208); 
					code[i++] = LOADW;
					}
					break;
				case 13 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:39:6: l= LABEL COL
					{
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly230); 
					match(input,COL,FOLLOW_COL_in_assembly232); 
					labelAdd.put((l!=null?l.getText():null),i);
					}
					break;
				case 14 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:40:6: BRANCH l= LABEL
					{
					match(input,BRANCH,FOLLOW_BRANCH_in_assembly245); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly249); 
					code[i++] = BRANCH;
					                       labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 15 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:42:6: BRANCHEQ l= LABEL
					{
					match(input,BRANCHEQ,FOLLOW_BRANCHEQ_in_assembly259); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly263); 
					code[i++] = BRANCHEQ; //
					                        labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 16 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:44:6: BRANCHLESSEQ l= LABEL
					{
					match(input,BRANCHLESSEQ,FOLLOW_BRANCHLESSEQ_in_assembly272); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly276); 
					code[i++] = BRANCHLESSEQ;
					                          labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 17 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:46:7: BRANCHGREATEQ l= LABEL
					{
					match(input,BRANCHGREATEQ,FOLLOW_BRANCHGREATEQ_in_assembly286); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly290); 
					code[i++] = BRANCHGREATEQ;
					                          labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 18 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:48:6: JS
					{
					match(input,JS,FOLLOW_JS_in_assembly299); 
					code[i++] = JS;
					}
					break;
				case 19 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:49:6: LOADRA
					{
					match(input,LOADRA,FOLLOW_LOADRA_in_assembly328); 
					code[i++] = LOADRA;
					}
					break;
				case 20 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:50:6: STORERA
					{
					match(input,STORERA,FOLLOW_STORERA_in_assembly350); 
					code[i++] = STORERA;
					}
					break;
				case 21 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:51:6: LOADRV
					{
					match(input,LOADRV,FOLLOW_LOADRV_in_assembly370); 
					code[i++] = LOADRV;
					}
					break;
				case 22 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:52:6: STORERV
					{
					match(input,STORERV,FOLLOW_STORERV_in_assembly391); 
					code[i++] = STORERV;
					}
					break;
				case 23 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:53:6: LOADFP
					{
					match(input,LOADFP,FOLLOW_LOADFP_in_assembly412); 
					code[i++] = LOADFP;
					}
					break;
				case 24 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:54:6: STOREFP
					{
					match(input,STOREFP,FOLLOW_STOREFP_in_assembly433); 
					code[i++] = STOREFP;
					}
					break;
				case 25 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:55:6: COPYFP
					{
					match(input,COPYFP,FOLLOW_COPYFP_in_assembly453); 
					code[i++] = COPYFP;
					}
					break;
				case 26 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:56:6: LOADHP
					{
					match(input,LOADHP,FOLLOW_LOADHP_in_assembly474); 
					code[i++] = LOADHP;
					}
					break;
				case 27 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:57:6: STOREHP
					{
					match(input,STOREHP,FOLLOW_STOREHP_in_assembly495); 
					code[i++] = STOREHP;
					}
					break;
				case 28 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:58:6: PRINT
					{
					match(input,PRINT,FOLLOW_PRINT_in_assembly515); 
					code[i++] = PRINT;
					}
					break;
				case 29 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\SVM.g:59:6: HALT
					{
					match(input,HALT,FOLLOW_HALT_in_assembly534); 
					code[i++] = HALT;
					}
					break;

				default :
					break loop1;
				}
			}

			 for (Integer refAdd: labelRef.keySet()) {
				              code[refAdd]=labelAdd.get(labelRef.get(refAdd));
					     } 
					   
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "assembly"

	// Delegated rules



	public static final BitSet FOLLOW_PUSH_in_assembly42 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_NUMBER_in_assembly46 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_PUSH_in_assembly57 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_LABEL_in_assembly61 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_POP_in_assembly81 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_ADD_in_assembly96 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_SUB_in_assembly110 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_MULT_in_assembly124 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_DIV_in_assembly137 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_AND_in_assembly151 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_OR_in_assembly166 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_NOT_in_assembly181 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_STOREW_in_assembly196 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LOADW_in_assembly208 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LABEL_in_assembly230 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COL_in_assembly232 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_BRANCH_in_assembly245 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_LABEL_in_assembly249 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_BRANCHEQ_in_assembly259 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_LABEL_in_assembly263 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_BRANCHLESSEQ_in_assembly272 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_LABEL_in_assembly276 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_BRANCHGREATEQ_in_assembly286 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_LABEL_in_assembly290 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_JS_in_assembly299 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LOADRA_in_assembly328 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_STORERA_in_assembly350 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LOADRV_in_assembly370 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_STORERV_in_assembly391 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LOADFP_in_assembly412 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_STOREFP_in_assembly433 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_COPYFP_in_assembly453 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_LOADHP_in_assembly474 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_STOREHP_in_assembly495 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_PRINT_in_assembly515 = new BitSet(new long[]{0x00000007FEFFDBF2L});
	public static final BitSet FOLLOW_HALT_in_assembly534 = new BitSet(new long[]{0x00000007FEFFDBF2L});
}
