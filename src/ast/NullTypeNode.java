package ast;

public class NullTypeNode implements Node {
  
  public NullTypeNode () {
  }
  
  public String toPrint(String s) {
	return s+"NullType\n";  
  }
    
  //non utilizzato
  public Node typeCheck() {
    return null;
  }
  
  // non utilizzato
  public String codeGeneration() {
	  return "";
  }
    
}  
