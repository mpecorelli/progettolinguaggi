package ast;
public class ParNode implements DecNode {

  private String id;
  private Node type;
  
  public ParNode (String i, Node t) {
   id=i;
   type=t;
  }
  
  @Override
  public String toPrint(String s) {
	  return s+"Par:" + id +"\n"
			 +type.toPrint(s+"  ") ; 
  }
  
  @Override
  public Node typeCheck () {
     return null;
  }
  
  //non utilizzato -> i parametri li alloca sullo stack il chiamante, il chiamato genera codice solo sui valori locali -> questo non serve
  @Override
  public String codeGeneration() {
		return "";
  }

  @Override
  public Node getSymType() {
	  return type;
  }
    
}  