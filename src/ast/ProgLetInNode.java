package ast;
import java.util.ArrayList;
import lib.*;

public class ProgLetInNode implements Node {

  private ArrayList<DecNode> declist;
  private Node exp;
  
  public ProgLetInNode (ArrayList<DecNode> d, Node e) {
    declist=d;
    exp=e;
  }
  
  public String toPrint(String s) {
	String declstr="";
    for (DecNode dec:declist)
      declstr+=dec.toPrint(s+"  ");
	return s+"ProgLetIn\n" + declstr + exp.toPrint(s+"  ") ; 
  }
  
  public Node typeCheck () {
    for (DecNode dec:declist)
      dec.typeCheck();
    return exp.typeCheck();
  }
  
  public String codeGeneration() {
	  String decListCode="";
	  
	  for (DecNode dec:declist)
		  decListCode+=dec.codeGeneration();
	  
	  return  "push 0\n"+
	  		  decListCode+
			  exp.codeGeneration()+
			  "halt\n"+
			  FOOLlib.getCode();
  } 
  
  
    
}  