package ast;

public class NullNode implements Node {

  public NullNode(){}
  
  @Override
  public String toPrint(String s) {
    return s+"Null:" ;
  }
  
  @Override
  public Node typeCheck() {
    return new NullTypeNode();
  }

  @Override
  public String codeGeneration() {
	  // SOLO SE FACCIAMO GLI OGGETTI QUESTA CLASSE SERVE; SE NO LA CANCELLO
	return ""; 
  }    
         
} 
