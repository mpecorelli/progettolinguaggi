package ast;
public class IdNode implements Node {

  private String id;
  private STentry entry;
  private int nestinglevel;
  
  public IdNode (String i, STentry st, int nl) {
    id=i;
    entry=st;
    nestinglevel=nl;
  }
  
  public String toPrint(String s) {
	return s+"Id:" + id + " at nestlev " + nestinglevel +"\n" + entry.toPrint(s+"  ") ;  
  }
  
  public Node typeCheck () {
//	  System.out.println("Id: " + id + "Id Type : " + entry.getType());
	/*if (entry.getType() instanceof ArrowTypeNode) { 
	  System.out.println("Wrong usage of function identifier");
      System.exit(0);
    }	 */
    return entry.getType();
  }
  
  public String codeGeneration() {
      String getAR="";
      String code = "push "+entry.getOffset()+"\n"+ //calcola indirizzo offset+$fp
		       "lfp\n"+getAR+ //risalgo la catena statica
			   "add\n"+ 
               "lw\n";
      
	  for (int i=0; i<nestinglevel-entry.getNestinglevel(); i++) 
	    	 getAR+="lw\n";
	  
	  if ( entry.getType() instanceof ArrowTypeNode){
		  code += "push "+( entry.getOffset() - 1) +"\n"+ //calcola indirizzo offset+$fp
		       "lfp\n"+getAR+ //risalgo la catena statica
			   "add\n"+ 
               "lw\n";  //carica sullo stack il valore a quell'indirizzo
	  }
	    return  code;
  }
}  