package ast;

import java.util.List;

public class ArrowTypeNode implements Node {

  private List<Node> parlist; 
  private Node ret;
  
  public ArrowTypeNode (List<Node> p, Node r) {
    parlist=p;
    ret=r;
  }
    
  public String toPrint(String s) { 
	String parlstr="";
    for (Node par:parlist)
      parlstr+=par.toPrint(s+"  ");
	return s+"ArrowType\n" + parlstr + ret.toPrint(s+"  ->") ; 
  }
  
  public Node getRet () { 
    return ret;
  }
  
  public List<Node> getParList () { 
    return parlist;
  }

  // qua prima era null, non so se serve
  public Node typeCheck () {
    return this;
  }

  public String codeGeneration() {
		return "";
  }
  
  @Override
  public String toString(){
	  return " ArrowType ";
  }

}  