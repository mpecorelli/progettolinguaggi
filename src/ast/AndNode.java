package ast;

import lib.FOOLlib;

public class AndNode implements Node {

	private Node left;
	private Node right;

	public AndNode (Node l, Node r) {
		left=l;
		right=r;
	}

	public String toPrint(String s) {
		return s+"And\n" + left.toPrint(s+"  ")  
		+ right.toPrint(s+"  ") ; 
	}

	public Node typeCheck() {
		if (! ( FOOLlib.isSubtype(left.typeCheck(),new IntTypeNode()) &&
				FOOLlib.isSubtype(right.typeCheck(),new IntTypeNode()) ) ) {
			System.out.println("Non integers in And");
			System.exit(0);
		}
		return new BoolTypeNode();
	}  

	  public String codeGeneration() {
			return left.codeGeneration()+
				   right.codeGeneration()+
				   "and\n";
	  }

}  
