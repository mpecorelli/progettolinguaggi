package ast;
//import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import lib.FOOLlib;

public class CallNode implements Node {

  private String id;
  private STentry entry; 
  private ArrayList<Node> parlist; 
  private int nestinglevel;

  
  public CallNode (String i, STentry e, ArrayList<Node> p, int nl) {
    id=i;
    entry=e;
    parlist = p;
    nestinglevel=nl;
  }
  
  public String toPrint(String s) {  
    String parlstr="";
	for (Node par:parlist)
	  parlstr+=par.toPrint(s+"  ");		
	return s+"Call:" + id + " at nestlev " + nestinglevel +"\n" 
           +entry.toPrint(s+"  ")
           +parlstr;        
  }
  
  public Node typeCheck () {                             
	 ArrowTypeNode t=null;
//	 System.out.println("CallType: " + entry.getType());
     if (entry.getType() instanceof ArrowTypeNode) t=(ArrowTypeNode) entry.getType(); 
     else {
       System.out.println("Invocation of a non-function "+id);
       System.exit(0);
     }
     
     //controllo il numero dei parametri
     List<Node> p = t.getParList();
     if ( !(p.size() == parlist.size()) ) {
       System.out.println("Wrong number of parameters in the invocation of "+id);
       System.exit(0);
     } 
     
     
     for (int i=0; i<parlist.size(); i++) {
       Node parType = (parlist.get(i)).typeCheck();
       Node decType = p.get(i);
       if ( (decType instanceof ArrowTypeNode && !(parType instanceof ArrowTypeNode)) || !(FOOLlib.isSubtype( parType, decType) ) ) {
         System.out.println("Wrong type for "+(i+1)+"-th parameter in the invocation of " + id + " [used " + parType.toString() + " instead of " + decType.toString() + "]");
         System.exit(0);
       } 
     }
     
     return t.getRet();
     
  }
  
  public String codeGeneration() {
	  
	  String parCode = ""; 
	  
	  for( int i = parlist.size() - 1; i >= 0; i --){
		  parCode += parlist.get(i).codeGeneration();
	  }
	  
	//ora gestiamo il nesting e la risalita della catena statica
      String getAR="";
	  for (int i=0; i<nestinglevel-entry.getNestinglevel(); i++) 
	    	 getAR+= "lw\n";
	  
//	  System.out.println(entry.getOffset());
	  
		return "lfp"   + "\n" +          //control link al chiamante
			   
				parCode + 
			   				//Recupera FP ad AR dichiarazione funzione (Per settare l'access link)
				"lfp\n"+
				getAR+
				"push "+entry.getOffset()+"\n"+ 
				"add\n"+
				//"print\n" +
				"lw\n"+
				
				//Recupera indir funzione (Per saltare al codice della funzione)
				"lfp\n"+
				getAR+
				"push "+(entry.getOffset() - 1)+"\n"+
				"add\n"+
//				"print\n" +
				"lw\n"+	
				
			   /*
			    *        "lfp\n"   +  
			   
		       getAR + 	//Al            //ora recupero l'indirizzo a cui la funzione deve saltare e lo metto sullo stack
			   "push " + entry.getOffset() + "\n" + //calcola indirizzo offset+$fp
		       "print\n" +
			   
			   "lfp"   + "\n" + 
			   getAR +       //risalgo la catena statica
			   "add"   + "\n" + 
			   "print\n" +
			   "lw"    + "\n" +               //carica sullo stack l'indirizzo a cui saltare;
			   
			   */
			   //"print\n" +
               "js"    + "\n" ;		      //salta a quell'indirizzo -> questo anche carica nel registro ra l'indirizzo dell'istruzione successiva quando torno indietro
  
		/// --- Fino a qua ho creato una parte dell'ar (quella del chiamante), fino al return address (che adesso � nel registro ar)
		/// --- la seconda met� la deve fare il chiamato --> FunNode
  }

    
}  