package ast;

import ast.Node;
import lib.FOOLlib;

public class NotNode implements Node {

	  private Node n;
	  
	  public NotNode (Node n) {
	    this.n = n;
	  }
	  
	  public String toPrint(String s) {
	    return s+"Not\n" + n.toPrint(s+"  ");
	  }
	  
	  public Node typeCheck() {
		  
	    if (! ( FOOLlib.isSubtype( n.typeCheck(),new BoolTypeNode() ) ) ) {
	      System.out.println("Not Boolean Type in Not");
	      System.exit(0);
	    }
	    
	    return new BoolTypeNode();
	  }  
	  
	  public String codeGeneration() {
			return n.codeGeneration()+
				   "not\n";
	  }
}
