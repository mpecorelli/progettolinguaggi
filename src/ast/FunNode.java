package ast;
import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements DecNode {

  private String id;
  private Node retType; 
  private ArrayList<ParNode> parlist = new ArrayList<ParNode>(); 
  private ArrayList<DecNode> declist; 
  private Node body;
  private Node type;
  
  public FunNode (String i, Node t) {
    id=i;
    retType=t;
    type = new ArrowTypeNode(new ArrayList<>(), retType);
  }
  
  public void addDecBody (ArrayList<DecNode> d, Node b) {
    declist=d;
    body=b;
  }  
  
  public void addPar (ParNode p) {
    parlist.add(p);
  }  
  
  public String toPrint(String s) {
	String parlstr="";
	for (ParNode par:parlist)
	  parlstr+=par.toPrint(s+"  ");
	String declstr="";
	if (declist!=null) 
	  for (DecNode dec:declist)
	    declstr+=dec.toPrint(s+"  ");
    return s+"Fun:" + id +"\n"
		   +retType.toPrint(s+"  ")
		   +parlstr
	   	   +declstr
           +body.toPrint(s+"  ") ; 
  }
  
  //valore di ritorno non utilizzato
  public Node typeCheck () {
	if (declist!=null) 
	  for (DecNode dec:declist)
		dec.typeCheck();
    if ( !(FOOLlib.isSubtype(body.typeCheck(),retType)) ){
      System.out.println("Wrong return type for function "+id);
      System.exit(0);
    }  
    return null;
  }
  
  public String codeGeneration() {
	  
	  	String decListCode="";
	  	
	  	if (declist != null){
	  		for (Node dec:declist){
	  			decListCode+=dec.codeGeneration();
	  		}
	  	}
	  	
	  	//faccio una pop per ogni dichiarazione locale <-- dovr� distruggere lo stack dopo la chiamata
	  	String popDecl = "";
	  	
	  	if (declist != null){
	  		for (DecNode dec:declist){
//	  			System.out.println("Fun id" + dec.toPrint("") + " type: " + dec.getSymType());
	  			if (dec.getSymType() instanceof ArrowTypeNode){
	  				popDecl+="pop\n";
	  			}
	  			popDecl+="pop\n";
	  		}
	  	}
	  	
	  	//faccio una pop per ogni parametro <-- dovr� distruggere lo stack dopo la chiamata
	  	String popParl = "";
	  	
	  	if (parlist != null){
	  		for (ParNode par:parlist){
	  			if (par.getSymType() instanceof ArrowTypeNode){
	  				popParl+="pop\n";
	  			}
	  			popParl+="pop\n";
	  		}
	  	}
	  	
	    String funl=FOOLlib.freshFunLabel(); 
	    
	    FOOLlib.putCode(funl+":\n"   +
	                    "cfp" + "\n" + // copia $sp in $fp
	                    "lra" + "\n" + // inserisce nello stack il return address
	                    decListCode  + 
	                    
	                    // -- fine AR 
	                    
	                    body.codeGeneration() + 		//codice della funzione
	                    
	                    // adesso ho finito la mia funzione, devo ripulire lo stack dall'ar per mantenere l'invariante -> smonto lo stack pezzo per pezzo
	                    
	                    "srv" + "\n" +		//salvo in un registro il return value della funzione ( servirà alla fine)
	                    popDecl + 			//pulisco lo stack dalle chiamate locali ( 1 pop per ogni variabile)
	                    "sra" + "\n" +		// pop del return address (salvandolo in ra)
	                    "pop" + "\n" +		// butto via l'access link
	                    popParl + 			//pulisco lo stack dai parametri ( 1 pop per ogni parametro)
	                    "sfp" + "\n" +		// setto $fp al valore del control link
	                    "lrv" + "\n" + 			// metto in cima allo stack il risultato della funzione
	                    "lra" + "\n" +			// metto l'indirizzo a cui saltare sullo stack ( per un attimo solo) 
	                    "js"  + "\n" 			// salta all'indirizzo che ho appena messo ( e lo poppa ) 
	    		);
	    
	    // la chiamata di una funzione � una qualsiasi espressione, quindi deve valere l'invariante
	    
		return "lfp\n" + "push "+ funl +"\n";
  }
  
  @Override
  public Node getSymType() {
	  	return type;
  }
}  