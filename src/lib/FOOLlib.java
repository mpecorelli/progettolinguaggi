package lib;

import java.util.List;

import ast.*;

public class FOOLlib {
  
  private static int labCount=0; 
  
  private static int funLabCount=0; 

  private static String funCode=""; 

  public static boolean isSubtype (Node a, Node b) {
	  
//	  System.out.print("Contollo i sottotipi di : " + a.toString() + " - " + b.toString());
	  
	  Node fa = null;
	  Node fb = null;
	  
	  boolean parTypes = true;
	  
	  if ( a instanceof ArrowTypeNode && b instanceof ArrowTypeNode ){
		  parTypes = isParameterSupertype(((ArrowTypeNode) a).getParList(), ((ArrowTypeNode) b).getParList());
		  
	  }
	  
	  if ( a instanceof ArrowTypeNode) 
		  fa = ((ArrowTypeNode) a).getRet();
	  else 
		  fa = a;
	  if ( b instanceof ArrowTypeNode) 
		  fb = ((ArrowTypeNode) b).getRet();
	  else 
		  fb = b;
	  
    boolean retType =  fa.getClass().equals(fb.getClass()) ||( (fa instanceof BoolTypeNode) && (fb instanceof IntTypeNode) ); 
    
    boolean res = retType && parTypes;
    
//    System.out.println(" controllo: " + res);
    
    return res;
  } 
  
  //Controlla la controvarianza dei parametri dei ArrowType -> devono essere super tipi per poter fare override di metodi con tipi di ritorno/parametri diversi (come vogliamo noi)
  private static boolean isParameterSupertype(List<Node> aPar, List<Node> bPar){
	  
	  if ( aPar.size() != bPar.size()) return false;
	  
	  for (int i = 0; i < aPar.size(); i ++){
		  if (!isSubtype(bPar.get(i), aPar.get(i)))
			  return false;
	  }
	  
	  return true;
  }
    
  public static String freshLabel() { 
	return "label"+(labCount++);
  } 

  public static String freshFunLabel() { 
	return "function"+(funLabCount++);
  } 
  
  public static void putCode(String c) { 
    funCode+="\n"+c; //aggiunge una linea vuota di separazione prima di una funzione
  } 
  
  public static String getCode() { 
    return funCode;
  } 


}