// $ANTLR 3.5.2 C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g 2017-05-24 14:35:52

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GE", "ID", "IF", "IN", 
		"INT", "INTEGER", "LE", "LET", "LPAR", "MINUS", "NEW", "NOT", "NULL", 
		"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", 
		"WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g"; }


	private int nestingLevel = -1;
	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)



	// $ANTLR start "prog"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:24:1: prog returns [Node ast] : (e= exp SEMIC | LET d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<DecNode> d =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:25:2: (e= exp SEMIC | LET d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||(LA1_0 >= NEW && LA1_0 <= NULL)||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:25:10: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog47);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog49); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:26:11: LET d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog63); 
					nestingLevel++;
					             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					             symTable.add(hm);
					            
					pushFollow(FOLLOW_declist_in_prog92);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog94); 
					pushFollow(FOLLOW_exp_in_prog98);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog100); 
					symTable.remove(nestingLevel--);
					             ast = new ProgLetInNode(d,e) ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "cllist"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:1: cllist : ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )* ;
	public final void cllist() throws RecognitionException {
		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:9: ( ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )* )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:11: ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )*
			{
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:11: ( CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==CLASS) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:13: CLASS ID ( EXTENDS ID )? LPAR ( ID COLON basic ( COMMA ID COLON basic )* )? RPAR CLPAR ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )* CRPAR
					{
					match(input,CLASS,FOLLOW_CLASS_in_cllist134); 
					match(input,ID,FOLLOW_ID_in_cllist136); 
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:22: ( EXTENDS ID )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==EXTENDS) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:23: EXTENDS ID
							{
							match(input,EXTENDS,FOLLOW_EXTENDS_in_cllist139); 
							match(input,ID,FOLLOW_ID_in_cllist141); 
							}
							break;

					}

					match(input,LPAR,FOLLOW_LPAR_in_cllist145); 
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:41: ( ID COLON basic ( COMMA ID COLON basic )* )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==ID) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:42: ID COLON basic ( COMMA ID COLON basic )*
							{
							match(input,ID,FOLLOW_ID_in_cllist148); 
							match(input,COLON,FOLLOW_COLON_in_cllist150); 
							pushFollow(FOLLOW_basic_in_cllist152);
							basic();
							state._fsp--;

							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:57: ( COMMA ID COLON basic )*
							loop3:
							while (true) {
								int alt3=2;
								int LA3_0 = input.LA(1);
								if ( (LA3_0==COMMA) ) {
									alt3=1;
								}

								switch (alt3) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:38:58: COMMA ID COLON basic
									{
									match(input,COMMA,FOLLOW_COMMA_in_cllist155); 
									match(input,ID,FOLLOW_ID_in_cllist157); 
									match(input,COLON,FOLLOW_COLON_in_cllist159); 
									pushFollow(FOLLOW_basic_in_cllist161);
									basic();
									state._fsp--;

									}
									break;

								default :
									break loop3;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_cllist168); 
					match(input,CLPAR,FOLLOW_CLPAR_in_cllist188); 
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:18: ( FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==FUN) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:20: FUN ID COLON basic LPAR ( ID COLON type ( COMMA ID COLON type )* )? RPAR ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )? exp SEMIC
							{
							match(input,FUN,FOLLOW_FUN_in_cllist209); 
							match(input,ID,FOLLOW_ID_in_cllist211); 
							match(input,COLON,FOLLOW_COLON_in_cllist213); 
							pushFollow(FOLLOW_basic_in_cllist215);
							basic();
							state._fsp--;

							match(input,LPAR,FOLLOW_LPAR_in_cllist217); 
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:44: ( ID COLON type ( COMMA ID COLON type )* )?
							int alt6=2;
							int LA6_0 = input.LA(1);
							if ( (LA6_0==ID) ) {
								alt6=1;
							}
							switch (alt6) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:45: ID COLON type ( COMMA ID COLON type )*
									{
									match(input,ID,FOLLOW_ID_in_cllist220); 
									match(input,COLON,FOLLOW_COLON_in_cllist222); 
									pushFollow(FOLLOW_type_in_cllist224);
									type();
									state._fsp--;

									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:59: ( COMMA ID COLON type )*
									loop5:
									while (true) {
										int alt5=2;
										int LA5_0 = input.LA(1);
										if ( (LA5_0==COMMA) ) {
											alt5=1;
										}

										switch (alt5) {
										case 1 :
											// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:40:60: COMMA ID COLON type
											{
											match(input,COMMA,FOLLOW_COMMA_in_cllist227); 
											match(input,ID,FOLLOW_ID_in_cllist229); 
											match(input,COLON,FOLLOW_COLON_in_cllist231); 
											pushFollow(FOLLOW_type_in_cllist233);
											type();
											state._fsp--;

											}
											break;

										default :
											break loop5;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_cllist240); 
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:41:24: ( LET ( VAR ID COLON basic ASS exp SEMIC )* IN )?
							int alt8=2;
							int LA8_0 = input.LA(1);
							if ( (LA8_0==LET) ) {
								alt8=1;
							}
							switch (alt8) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:41:25: LET ( VAR ID COLON basic ASS exp SEMIC )* IN
									{
									match(input,LET,FOLLOW_LET_in_cllist266); 
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:41:29: ( VAR ID COLON basic ASS exp SEMIC )*
									loop7:
									while (true) {
										int alt7=2;
										int LA7_0 = input.LA(1);
										if ( (LA7_0==VAR) ) {
											alt7=1;
										}

										switch (alt7) {
										case 1 :
											// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:41:30: VAR ID COLON basic ASS exp SEMIC
											{
											match(input,VAR,FOLLOW_VAR_in_cllist269); 
											match(input,ID,FOLLOW_ID_in_cllist271); 
											match(input,COLON,FOLLOW_COLON_in_cllist273); 
											pushFollow(FOLLOW_basic_in_cllist275);
											basic();
											state._fsp--;

											match(input,ASS,FOLLOW_ASS_in_cllist277); 
											pushFollow(FOLLOW_exp_in_cllist279);
											exp();
											state._fsp--;

											match(input,SEMIC,FOLLOW_SEMIC_in_cllist281); 
											}
											break;

										default :
											break loop7;
										}
									}

									match(input,IN,FOLLOW_IN_in_cllist285); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_cllist289);
							exp();
							state._fsp--;

							match(input,SEMIC,FOLLOW_SEMIC_in_cllist311); 
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,CRPAR,FOLLOW_CRPAR_in_cllist363); 
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "cllist"



	// $ANTLR start "declist"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:50:1: declist returns [ArrayList<DecNode> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<DecNode> declist() throws RecognitionException {
		ArrayList<DecNode> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<DecNode> d =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:51:2: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:51:4: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{
			astlist = new ArrayList<DecNode>() ; int offset=-2;
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:52:8: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==FUN||LA15_0==VAR) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:52:10: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:52:10: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==VAR) ) {
						alt14=1;
					}
					else if ( (LA14_0==FUN) ) {
						alt14=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 14, 0, input);
						throw nvae;
					}

					switch (alt14) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:53:13: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist434); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist438); 
							match(input,COLON,FOLLOW_COLON_in_declist440); 
							pushFollow(FOLLOW_type_in_declist444);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist446); 
							pushFollow(FOLLOW_exp_in_declist450);
							e=exp();
							state._fsp--;

							VarNode v = new VarNode((i!=null?i.getText():null),t,e); 
							               astlist.add(v); 
							               HashMap<String,STentry> hm = symTable.get(nestingLevel); 
							               if ( hm.put((i!=null?i.getText():null),new STentry(nestingLevel,t,offset--)) != null  ) 
							                 {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);} 
							               if(t instanceof ArrowTypeNode) offset--; 
							              
							}
							break;
						case 2 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:64:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist512); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist516); 
							match(input,COLON,FOLLOW_COLON_in_declist518); 
							pushFollow(FOLLOW_type_in_declist522);
							t=type();
							state._fsp--;

							//inserimento di ID nella symtable
							               FunNode f = new FunNode((i!=null?i.getText():null),t);
//							               System.out.println("tipo funzione: " + t);
							               astlist.add(f);
							               HashMap<String,STentry> hm = symTable.get(nestingLevel);
							               STentry entry = new STentry(nestingLevel,offset--); //separo introducendo "entry"
							               offset--;
							               if ( hm.put((i!=null?i.getText():null),entry) != null )
							                 {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);}
							                  //creare una nuova hashmap per la symTable
							               nestingLevel++;
							               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
							               symTable.add(hmn);
							              
							match(input,LPAR,FOLLOW_LPAR_in_declist569); 
							ArrayList<Node> parTypes = new ArrayList<Node>();
							                    int paroffset=1;
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:84:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==ID) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:84:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist614); 
									match(input,COLON,FOLLOW_COLON_in_declist616); 
									pushFollow(FOLLOW_type_in_declist620);
									fty=type();
									state._fsp--;

									 
									                  parTypes.add(fty); //
									                  ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
									                  f.addPar(fpar);
									                  if (fty instanceof ArrowTypeNode) paroffset++;  // se ho un parametro funzionale devo tenere libero un altro spazio (aumento di 1 l'offset) 
									                  if ( hmn.put((fid!=null?fid.getText():null),new STentry(nestingLevel,fty,paroffset++)) != null  )
									                    {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
									                     System.exit(0);}
									                  
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:95:19: ( COMMA id= ID COLON ty= type )*
									loop11:
									while (true) {
										int alt11=2;
										int LA11_0 = input.LA(1);
										if ( (LA11_0==COMMA) ) {
											alt11=1;
										}

										switch (alt11) {
										case 1 :
											// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:95:20: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist680); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist684); 
											match(input,COLON,FOLLOW_COLON_in_declist686); 
											pushFollow(FOLLOW_type_in_declist690);
											ty=type();
											state._fsp--;


											                    parTypes.add(ty); //
											                    ParNode par = new ParNode((id!=null?id.getText():null),ty);
											                    f.addPar(par);
											                    if (ty instanceof ArrowTypeNode) paroffset++; 
											                    if ( hmn.put((id!=null?id.getText():null),new STentry(nestingLevel,ty,paroffset++)) != null  )
											                      {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
											                       System.exit(0);}
											                    
											}
											break;

										default :
											break loop11;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist769); 
							entry.addType( new ArrowTypeNode(parTypes, t) );
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:108:15: ( LET d= declist IN )?
							int alt13=2;
							int LA13_0 = input.LA(1);
							if ( (LA13_0==LET) ) {
								alt13=1;
							}
							switch (alt13) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:108:16: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist789); 
									pushFollow(FOLLOW_declist_in_declist793);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist795); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist801);
							e=exp();
							state._fsp--;

							//chiudere scope
							              symTable.remove(nestingLevel--);
							              f.addDecBody(d,e);
							              
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist832); 
					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "exp"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:117:1: exp returns [Node ast] : t= term ( PLUS p= term | MINUS m= term | OR o= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node t =null;
		Node p =null;
		Node m =null;
		Node o =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:118:3: (t= term ( PLUS p= term | MINUS m= term | OR o= term )* )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:118:13: t= term ( PLUS p= term | MINUS m= term | OR o= term )*
			{
			pushFollow(FOLLOW_term_in_exp881);
			t=term();
			state._fsp--;

			ast =t;
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:119:5: ( PLUS p= term | MINUS m= term | OR o= term )*
			loop16:
			while (true) {
				int alt16=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt16=1;
					}
					break;
				case MINUS:
					{
					alt16=2;
					}
					break;
				case OR:
					{
					alt16=3;
					}
					break;
				}
				switch (alt16) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:120:7: PLUS p= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp897); 
					pushFollow(FOLLOW_term_in_exp902);
					p=term();
					state._fsp--;

					ast = new PlusNode (ast, p);
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:121:7: MINUS m= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp913); 
					pushFollow(FOLLOW_term_in_exp917);
					m=term();
					state._fsp--;

					ast = new MinusNode (ast, m);
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:122:7: OR o= term
					{
					match(input,OR,FOLLOW_OR_in_exp927); 
					pushFollow(FOLLOW_term_in_exp934);
					o=term();
					state._fsp--;

					ast = new OrNode (ast, o);
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:126:1: term returns [Node ast] : f= factor ( TIMES t= factor | DIV d= factor | AND a= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node t =null;
		Node d =null;
		Node a =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:127:3: (f= factor ( TIMES t= factor | DIV d= factor | AND a= factor )* )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:127:15: f= factor ( TIMES t= factor | DIV d= factor | AND a= factor )*
			{
			pushFollow(FOLLOW_factor_in_term978);
			f=factor();
			state._fsp--;

			ast = f;
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:128:5: ( TIMES t= factor | DIV d= factor | AND a= factor )*
			loop17:
			while (true) {
				int alt17=4;
				switch ( input.LA(1) ) {
				case TIMES:
					{
					alt17=1;
					}
					break;
				case DIV:
					{
					alt17=2;
					}
					break;
				case AND:
					{
					alt17=3;
					}
					break;
				}
				switch (alt17) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:128:9: TIMES t= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term991); 
					pushFollow(FOLLOW_factor_in_term995);
					t=factor();
					state._fsp--;

					ast = new MultNode (ast,t);
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:129:9: DIV d= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term1008); 
					pushFollow(FOLLOW_factor_in_term1014);
					d=factor();
					state._fsp--;

					ast = new DivNode (ast,d);
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:130:9: AND a= factor
					{
					match(input,AND,FOLLOW_AND_in_term1027); 
					pushFollow(FOLLOW_factor_in_term1033);
					a=factor();
					state._fsp--;

					ast = new AndNode (ast,a);
					}
					break;

				default :
					break loop17;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:134:1: factor returns [Node ast] : v= value ( EQ e= value | GE g= value | LE l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node v =null;
		Node e =null;
		Node g =null;
		Node l =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:135:3: (v= value ( EQ e= value | GE g= value | LE l= value )* )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:135:10: v= value ( EQ e= value | GE g= value | LE l= value )*
			{
			pushFollow(FOLLOW_value_in_factor1069);
			v=value();
			state._fsp--;

			ast = v;
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:136:5: ( EQ e= value | GE g= value | LE l= value )*
			loop18:
			while (true) {
				int alt18=4;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt18=1;
					}
					break;
				case GE:
					{
					alt18=2;
					}
					break;
				case LE:
					{
					alt18=3;
					}
					break;
				}
				switch (alt18) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:136:7: EQ e= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor1079); 
					pushFollow(FOLLOW_value_in_factor1083);
					e=value();
					state._fsp--;

					ast = new EqualNode (ast,e);
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:137:7: GE g= value
					{
					match(input,GE,FOLLOW_GE_in_factor1094); 
					pushFollow(FOLLOW_value_in_factor1098);
					g=value();
					state._fsp--;

					ast = new GreaterEqualNode (ast,g);
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:138:7: LE l= value
					{
					match(input,LE,FOLLOW_LE_in_factor1109); 
					pushFollow(FOLLOW_value_in_factor1113);
					l=value();
					state._fsp--;

					ast = new LesserEqualNode (ast,e);
					}
					break;

				default :
					break loop18;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:142:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | NULL | NEW ID LPAR (e1= exp ( COMMA e2= exp )* )? RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR e= exp RPAR | PRINT LPAR e= exp RPAR | LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA aa= exp )* )? RPAR | DOT m= ID LPAR (ma= exp ( COMMA aa= exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Token m=null;
		Node e1 =null;
		Node e2 =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node e =null;
		Node fa =null;
		Node aa =null;
		Node ma =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:143:3: (n= INTEGER | TRUE | FALSE | NULL | NEW ID LPAR (e1= exp ( COMMA e2= exp )* )? RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR e= exp RPAR | PRINT LPAR e= exp RPAR | LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA aa= exp )* )? RPAR | DOT m= ID LPAR (ma= exp ( COMMA aa= exp )* )? RPAR )? )
			int alt26=10;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt26=1;
				}
				break;
			case TRUE:
				{
				alt26=2;
				}
				break;
			case FALSE:
				{
				alt26=3;
				}
				break;
			case NULL:
				{
				alt26=4;
				}
				break;
			case NEW:
				{
				alt26=5;
				}
				break;
			case IF:
				{
				alt26=6;
				}
				break;
			case NOT:
				{
				alt26=7;
				}
				break;
			case PRINT:
				{
				alt26=8;
				}
				break;
			case LPAR:
				{
				alt26=9;
				}
				break;
			case ID:
				{
				alt26=10;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:143:6: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value1146); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:145:5: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value1161); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:147:5: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value1177); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:149:5: NULL
					{
					match(input,NULL,FOLLOW_NULL_in_value1199); 
					ast = new NullNode();
					}
					break;
				case 5 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:151:5: NEW ID LPAR (e1= exp ( COMMA e2= exp )* )? RPAR
					{
					match(input,NEW,FOLLOW_NEW_in_value1218); 
					match(input,ID,FOLLOW_ID_in_value1220); 
					match(input,LPAR,FOLLOW_LPAR_in_value1222); 
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:151:17: (e1= exp ( COMMA e2= exp )* )?
					int alt20=2;
					int LA20_0 = input.LA(1);
					if ( (LA20_0==FALSE||(LA20_0 >= ID && LA20_0 <= IF)||LA20_0==INTEGER||LA20_0==LPAR||(LA20_0 >= NEW && LA20_0 <= NULL)||LA20_0==PRINT||LA20_0==TRUE) ) {
						alt20=1;
					}
					switch (alt20) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:151:18: e1= exp ( COMMA e2= exp )*
							{
							pushFollow(FOLLOW_exp_in_value1227);
							e1=exp();
							state._fsp--;

							ast = e1;
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:151:42: ( COMMA e2= exp )*
							loop19:
							while (true) {
								int alt19=2;
								int LA19_0 = input.LA(1);
								if ( (LA19_0==COMMA) ) {
									alt19=1;
								}

								switch (alt19) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:151:43: COMMA e2= exp
									{
									match(input,COMMA,FOLLOW_COMMA_in_value1232); 
									pushFollow(FOLLOW_exp_in_value1236);
									e2=exp();
									state._fsp--;

									ast = e2;
									}
									break;

								default :
									break loop19;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_value1245); 
					}
					break;
				case 6 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:153:5: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value1263); 
					pushFollow(FOLLOW_exp_in_value1267);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value1274); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1276); 
					pushFollow(FOLLOW_exp_in_value1280);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1282); 
					match(input,ELSE,FOLLOW_ELSE_in_value1289); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1291); 
					pushFollow(FOLLOW_exp_in_value1295);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1297); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 7 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:158:5: NOT LPAR e= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value1320); 
					match(input,LPAR,FOLLOW_LPAR_in_value1322); 
					pushFollow(FOLLOW_exp_in_value1326);
					e=exp();
					state._fsp--;

					ast = new NotNode(e);
					match(input,RPAR,FOLLOW_RPAR_in_value1330); 
					}
					break;
				case 8 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:160:5: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value1340); 
					match(input,LPAR,FOLLOW_LPAR_in_value1342); 
					pushFollow(FOLLOW_exp_in_value1346);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1348); 
					ast = new PrintNode(e);
					}
					break;
				case 9 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:162:5: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value1368); 
					pushFollow(FOLLOW_exp_in_value1372);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1374); 
					ast = e;
					}
					break;
				case 10 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:164:5: i= ID ( LPAR (fa= exp ( COMMA aa= exp )* )? RPAR | DOT m= ID LPAR (ma= exp ( COMMA aa= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value1395); 
					 //try to find the definition of that identifier
					    int j=nestingLevel;
					    STentry entry=null; 
					    while (j>=0 && entry==null)
					      entry=(symTable.get(j--)).get((i!=null?i.getText():null));
					    if (entry==null)
					      {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
					       System.exit(0);}               
					    ast = new IdNode((i!=null?i.getText():null),entry, nestingLevel);
					  
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:175:3: ( LPAR (fa= exp ( COMMA aa= exp )* )? RPAR | DOT m= ID LPAR (ma= exp ( COMMA aa= exp )* )? RPAR )?
					int alt25=3;
					int LA25_0 = input.LA(1);
					if ( (LA25_0==LPAR) ) {
						alt25=1;
					}
					else if ( (LA25_0==DOT) ) {
						alt25=2;
					}
					switch (alt25) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:176:9: LPAR (fa= exp ( COMMA aa= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value1415); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:177:9: (fa= exp ( COMMA aa= exp )* )?
							int alt22=2;
							int LA22_0 = input.LA(1);
							if ( (LA22_0==FALSE||(LA22_0 >= ID && LA22_0 <= IF)||LA22_0==INTEGER||LA22_0==LPAR||(LA22_0 >= NEW && LA22_0 <= NULL)||LA22_0==PRINT||LA22_0==TRUE) ) {
								alt22=1;
							}
							switch (alt22) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:177:10: fa= exp ( COMMA aa= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1430);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:178:11: ( COMMA aa= exp )*
									loop21:
									while (true) {
										int alt21=2;
										int LA21_0 = input.LA(1);
										if ( (LA21_0==COMMA) ) {
											alt21=1;
										}

										switch (alt21) {
										case 1 :
											// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:178:12: COMMA aa= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1445); 
											pushFollow(FOLLOW_exp_in_value1449);
											aa=exp();
											state._fsp--;

											argList.add(aa);
											}
											break;

										default :
											break loop21;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1476); 
							ast =new CallNode((i!=null?i.getText():null),entry,argList, nestingLevel);
							}
							break;
						case 2 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:182:11: DOT m= ID LPAR (ma= exp ( COMMA aa= exp )* )? RPAR
							{
							match(input,DOT,FOLLOW_DOT_in_value1499); 
							m=(Token)match(input,ID,FOLLOW_ID_in_value1503); 
							/*qua suppongo ci vada una ricerca tipo quella sopra per vedere se il metodo � definito e non ho vogia di farla adesso*/
							match(input,LPAR,FOLLOW_LPAR_in_value1527); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:185:11: (ma= exp ( COMMA aa= exp )* )?
							int alt24=2;
							int LA24_0 = input.LA(1);
							if ( (LA24_0==FALSE||(LA24_0 >= ID && LA24_0 <= IF)||LA24_0==INTEGER||LA24_0==LPAR||(LA24_0 >= NEW && LA24_0 <= NULL)||LA24_0==PRINT||LA24_0==TRUE) ) {
								alt24=1;
							}
							switch (alt24) {
								case 1 :
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:185:12: ma= exp ( COMMA aa= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1544);
									ma=exp();
									state._fsp--;

									argList.add(ma);
									// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:186:13: ( COMMA aa= exp )*
									loop23:
									while (true) {
										int alt23=2;
										int LA23_0 = input.LA(1);
										if ( (LA23_0==COMMA) ) {
											alt23=1;
										}

										switch (alt23) {
										case 1 :
											// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:186:14: COMMA aa= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1562); 
											pushFollow(FOLLOW_exp_in_value1566);
											aa=exp();
											state._fsp--;

											argList.add(aa);
											}
											break;

										default :
											break loop23;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1597); 
							ast =new CallMethodNode((i!=null?i.getText():null),entry,argList);
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "type"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:193:1: type returns [Node ast] : (b= basic |a= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;


		Node b =null;
		Node a =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:194:3: (b= basic |a= arrow )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==BOOL||LA27_0==ID||LA27_0==INT) ) {
				alt27=1;
			}
			else if ( (LA27_0==LPAR) ) {
				alt27=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:194:7: b= basic
					{
					pushFollow(FOLLOW_basic_in_type1637);
					b=basic();
					state._fsp--;

					ast = b;
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:195:7: a= arrow
					{
					pushFollow(FOLLOW_arrow_in_type1649);
					a=arrow();
					state._fsp--;

					ast = a;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "basic"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:198:1: basic returns [Node ast] : ( INT | BOOL | ID );
	public final Node basic() throws RecognitionException {
		Node ast = null;


		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:199:3: ( INT | BOOL | ID )
			int alt28=3;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt28=1;
				}
				break;
			case BOOL:
				{
				alt28=2;
				}
				break;
			case ID:
				{
				alt28=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			switch (alt28) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:199:7: INT
					{
					match(input,INT,FOLLOW_INT_in_basic1670); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:200:7: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_basic1693); 
					ast =new BoolTypeNode();
					}
					break;
				case 3 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:201:7: ID
					{
					match(input,ID,FOLLOW_ID_in_basic1716); 
					ast =new IdTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "basic"



	// $ANTLR start "arrow"
	// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:204:1: arrow returns [Node ast] : LPAR (t1= type ( COMMA t2= type )* )? RPAR ARROW b= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;


		Node t1 =null;
		Node t2 =null;
		Node b =null;

		try {
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:205:3: ( LPAR (t1= type ( COMMA t2= type )* )? RPAR ARROW b= basic )
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:205:5: LPAR (t1= type ( COMMA t2= type )* )? RPAR ARROW b= basic
			{
			match(input,LPAR,FOLLOW_LPAR_in_arrow1760); 
			List<Node> parList = new ArrayList<Node>();
			// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:206:3: (t1= type ( COMMA t2= type )* )?
			int alt30=2;
			int LA30_0 = input.LA(1);
			if ( (LA30_0==BOOL||LA30_0==ID||LA30_0==INT||LA30_0==LPAR) ) {
				alt30=1;
			}
			switch (alt30) {
				case 1 :
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:206:5: t1= type ( COMMA t2= type )*
					{
					pushFollow(FOLLOW_type_in_arrow1770);
					t1=type();
					state._fsp--;

					parList.add(t1);
					// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:206:37: ( COMMA t2= type )*
					loop29:
					while (true) {
						int alt29=2;
						int LA29_0 = input.LA(1);
						if ( (LA29_0==COMMA) ) {
							alt29=1;
						}

						switch (alt29) {
						case 1 :
							// C:\\Users\\ManuBottax\\Documents\\SourceTreeRepository\\ProgettoLcmcRepo\\FOOL.g:206:39: COMMA t2= type
							{
							match(input,COMMA,FOLLOW_COMMA_in_arrow1776); 
							pushFollow(FOLLOW_type_in_arrow1780);
							t2=type();
							state._fsp--;

							parList.add(t2);
							}
							break;

						default :
							break loop29;
						}
					}

					}
					break;

			}

			match(input,RPAR,FOLLOW_RPAR_in_arrow1789); 
			match(input,ARROW,FOLLOW_ARROW_in_arrow1791); 
			pushFollow(FOLLOW_basic_in_arrow1795);
			b=basic();
			state._fsp--;

			Node ret = b;
			ast = new ArrowTypeNode(parList, ret);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "arrow"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog47 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog49 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog63 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_prog92 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_prog94 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_prog98 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog100 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CLASS_in_cllist134 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist136 = new BitSet(new long[]{0x0000000040080000L});
	public static final BitSet FOLLOW_EXTENDS_in_cllist139 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist141 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist145 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist148 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist150 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist152 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist155 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist157 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist159 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist161 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist168 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_cllist188 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_FUN_in_cllist209 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist211 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist213 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist215 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist217 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist220 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist222 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist224 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist227 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist229 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist231 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist233 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist240 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_cllist266 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_VAR_in_cllist269 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist271 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist273 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist275 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_cllist277 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist279 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist281 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_IN_in_cllist285 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist289 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist311 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_CRPAR_in_cllist363 = new BitSet(new long[]{0x0000000000000102L});
	public static final BitSet FOLLOW_VAR_in_declist434 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist438 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist440 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist444 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist446 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist450 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_FUN_in_declist512 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist516 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist518 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist522 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_declist569 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_declist614 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist616 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist620 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist680 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist684 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist686 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist690 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist769 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_declist789 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_declist793 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_declist795 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist801 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist832 = new BitSet(new long[]{0x0000080000200002L});
	public static final BitSet FOLLOW_term_in_exp881 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_PLUS_in_exp897 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp902 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_MINUS_in_exp913 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp917 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_OR_in_exp927 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp934 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_factor_in_term978 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_TIMES_in_term991 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term995 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_DIV_in_term1008 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term1014 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_AND_in_term1027 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term1033 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_value_in_factor1069 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_EQ_in_factor1079 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1083 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_GE_in_factor1094 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1098 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_LE_in_factor1109 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1113 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_INTEGER_in_value1146 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value1161 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value1177 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULL_in_value1199 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_value1218 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value1220 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1222 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1227 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1232 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1236 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1245 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value1263 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1267 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_THEN_in_value1274 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1276 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1280 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1282 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value1289 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1291 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1295 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1297 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value1320 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1322 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1326 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1330 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value1340 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1342 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1346 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1348 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value1368 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1372 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1374 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value1395 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_LPAR_in_value1415 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1430 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1445 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1449 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1476 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_value1499 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value1503 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1527 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1544 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1562 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1566 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1597 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_basic_in_type1637 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrow_in_type1649 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_basic1670 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_basic1693 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_basic1716 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_arrow1760 = new BitSet(new long[]{0x0000004044800080L});
	public static final BitSet FOLLOW_type_in_arrow1770 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_arrow1776 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_arrow1780 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_arrow1789 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ARROW_in_arrow1791 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_arrow1795 = new BitSet(new long[]{0x0000000000000002L});
}
