grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
}

@lexer::members {
int lexicalErrors=0;
}

@members{
private int nestingLevel = -1;
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

/*------------------------------------------------------------------
 * GRAMMAR (PARSER) RULES
 *------------------------------------------------------------------*/
  
prog	returns [Node ast]
	:       e=exp SEMIC	{$ast = new ProgNode($e.ast);}
        | LET 
            {nestingLevel++;
             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
             symTable.add(hm);
            }
          d=declist IN e=exp SEMIC 
            {symTable.remove(nestingLevel--);
             $ast = new ProgLetInNode($d.astlist,$e.ast) ;} 
	;
	
	
	//lo facciamo dopo quando arriviamo agli oggetti SE NO LO TOGLIAMOOOOOOOOOOO
cllist  : ( CLASS ID (EXTENDS ID)? LPAR (ID COLON basic (COMMA ID COLON basic)* )? RPAR    
              CLPAR
                 ( FUN ID COLON basic LPAR (ID COLON type (COMMA ID COLON type)* )? RPAR
                       (LET (VAR ID COLON basic ASS exp SEMIC)* IN)? exp 
                   SEMIC
                 )*                
              CRPAR
          )*
        ; 
        
// ------------------------------------------------------------------------------------------

declist	returns [ArrayList<DecNode> astlist]
	: {$astlist= new ArrayList<DecNode>() ; int offset=-2;}
	      ( (
            VAR i=ID COLON t=type ASS e=exp
              {VarNode v = new VarNode($i.text,$t.ast,$e.ast); 
               $astlist.add(v); 
               HashMap<String,STentry> hm = symTable.get(nestingLevel); 
               if ( hm.put($i.text,new STentry(nestingLevel,$t.ast,offset--)) != null  ) 
                 {System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);} 
               if($t.ast instanceof ArrowTypeNode) offset--; 
              }  
              
            | 
            FUN i=ID COLON t=type
              {//inserimento di ID nella symtable
               FunNode f = new FunNode($i.text,$t.ast);
               $astlist.add(f);
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
               STentry entry = new STentry(nestingLevel,offset--); //separo introducendo "entry"
               offset--;
               if ( hm.put($i.text,entry) != null )
                 {System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);}
                  //creare una nuova hashmap per la symTable
               nestingLevel++;
               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
               symTable.add(hmn);
              }
              
              LPAR {ArrayList<Node> parTypes = new ArrayList<Node>();
                    int paroffset=1;} 
                    // un solo parametro
                (fid=ID COLON fty=type
                  { 
                  parTypes.add($fty.ast); //
                  ParNode fpar = new ParNode($fid.text,$fty.ast);
                  f.addPar(fpar);
                  if ($fty.ast instanceof ArrowTypeNode) paroffset++;  // se ho un parametro funzionale devo tenere libero un altro spazio (aumento di 1 l'offset) 
                  if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  )
                    {System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
                     System.exit(0);}
                  }
                  // altri parametri
                  (COMMA id=ID COLON ty=type
                    {
                    parTypes.add($ty.ast); //
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
                    if ($ty.ast instanceof ArrowTypeNode) paroffset++; 
                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  )
                      {System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                       System.exit(0);}
                    }
                  )*
                )? 
              RPAR {entry.addType( new ArrowTypeNode(parTypes, $t.ast) );} //
              (LET d=declist IN)? e=exp 
              {//chiudere scope
              symTable.remove(nestingLevel--);
              f.addDecBody($d.astlist,$e.ast);
              }
          ) SEMIC
        )+          
	;
	 
exp returns [Node ast]
  :         t=term {$ast=$t.ast;}
    (
      PLUS  p=term {$ast = new PlusNode ($ast, $p.ast);} 
    | MINUS m=term {$ast = new MinusNode ($ast, $m.ast);}
    | OR    o=term {$ast = new OrNode ($ast, $o.ast);}   
    )* 
  ;
  
term returns [Node ast]
  :           f=factor {$ast= $f.ast;} 
    (   TIMES t=factor {$ast= new MultNode ($ast,$t.ast);} 
      | DIV   d=factor {$ast= new DivNode ($ast,$d.ast);} 
      | AND   a=factor {$ast= new AndNode ($ast,$a.ast);} 
    )*
  ;
	
factor  returns [Node ast]
  :      v=value {$ast= $v.ast;}
    ( EQ e=value {$ast= new EqualNode ($ast,$e.ast);} 
    | GE g=value {$ast= new GreaterEqualNode ($ast,$g.ast);} 
    | LE l=value {$ast= new LesserEqualNode ($ast,$e.ast);} 
    )*
  ; 
 	
value returns [Node ast]
  :  n=INTEGER   {$ast= new IntNode(Integer.parseInt($n.text));}  
  
  | TRUE    {$ast= new BoolNode(true);}  
  
  | FALSE   {$ast= new BoolNode(false);}         
  
  | NULL    {$ast= new NullNode();}     
  
  | NEW ID LPAR (e1=exp {$ast= $e1.ast;} (COMMA e2=exp {$ast= $e2.ast;})* )? RPAR         
  
  | IF x=exp 
    THEN CLPAR y=exp CRPAR 
    ELSE CLPAR z=exp CRPAR 
    {$ast= new IfNode($x.ast,$y.ast,$z.ast);} 
        
  | NOT LPAR e=exp {$ast= new NotNode($e.ast);} RPAR 
  
  | PRINT LPAR e=exp RPAR  {$ast= new PrintNode($e.ast);} 
         
  | LPAR e=exp RPAR {$ast= $e.ast;}   
       
  | i=ID 
  { //try to find the definition of that identifier
    int j=nestingLevel;
    STentry entry=null; 
    while (j>=0 && entry==null)
      entry=(symTable.get(j--)).get($i.text);
    if (entry==null)
      {System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
       System.exit(0);}               
    $ast= new IdNode($i.text,entry, nestingLevel);
  }
  ( 
        LPAR {ArrayList<Node> argList = new ArrayList<Node>();}
        (fa=exp {argList.add($fa.ast);}
          (COMMA aa=exp {argList.add($aa.ast);})* 
        )? 
        RPAR {$ast=new CallNode($i.text,entry,argList, nestingLevel);}
        
        | DOT m=ID {/*qua suppongo ci vada una ricerca tipo quella sopra per vedere se il metodo � definito e non ho vogia di farla adesso*/} 
        
          LPAR {ArrayList<Node> argList = new ArrayList<Node>();}
          (ma=exp {argList.add($ma.ast);} 
            (COMMA aa=exp {argList.add($aa.ast);})* 
          )? 
          RPAR {$ast=new CallMethodNode($i.text,entry,argList);} //non so se serva un nodo diverso o meno, intanto lo creo poi al massimo lo tolgo
  )?     
  ; 
 	
 	
type returns [Node ast]
  :   b=basic {$ast = $b.ast;}
    | a=arrow {$ast = $a.ast;}
  ;

basic returns [Node ast]
  :   INT   {$ast=new IntTypeNode();}           
    | BOOL  {$ast=new BoolTypeNode();}            
    | ID    {$ast=new IdTypeNode();}                    
  ;  

arrow returns [Node ast]
  : LPAR {List<Node> parList = new ArrayList<Node>();}
  ( t1=type {parList.add($t1.ast);} ( COMMA t2=type {parList.add($t2.ast);})* )? RPAR ARROW b=basic {Node ret = $b.ast;} {$ast = new ArrowTypeNode(parList, ret);};
  	
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS    : '+' ;
MINUS   : '-' ;
TIMES    : '*' ;
DIV   : '/' ;
LPAR  : '(' ;
RPAR  : ')' ;
CLPAR : '{' ;
CRPAR : '}' ;
SEMIC   : ';' ;
COLON   : ':' ; 
COMMA : ',' ;
DOT : '.' ;
OR  : '||';
AND : '&&';
NOT : 'not' ;
GE  : '>=' ;
LE  : '<=' ;
EQ  : '==' ;  
ASS : '=' ;
TRUE  : 'true' ;
FALSE : 'false' ;
IF  : 'if' ;
THEN  : 'then';
ELSE  : 'else' ;
PRINT : 'print' ;
LET     : 'let' ; 
IN      : 'in' ;  
VAR     : 'var' ;
FUN : 'fun' ; 
CLASS : 'class' ; 
EXTENDS : 'extends' ; 
NEW   : 'new' ; 
NULL    : 'null' ;    
INT : 'int' ;
BOOL  : 'bool' ;
ARROW   : '->' ;  
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 

ID    : ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;

WHITESP : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;
 
ERR      : . { System.out.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ;

